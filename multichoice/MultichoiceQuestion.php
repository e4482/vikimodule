<?php


namespace vikimodule\multichoice;

use file_storage;
use InvalidArgumentException;
use vikimodule\Editor;
use vikimodule\Question;
use stored_file;

class MultichoiceQuestion extends Question
{
    /**
     * @var bool indicates if there is only one answer or if multiple answers can be selected.
     */
    private $singleanswer = false;

    private $answers;
    private $nbCorrectAnswers;

    public function __construct($idornull, string $name = '', Editor $question = null, array $answers = null, int $categoryid = 0)
    {
        if (func_num_args() == 1) {
            // Construction reading DB
            global $DB;
            parent::__construct($idornull);
            $this->singleanswer = $DB->get_record(
                'qtype_multichoice_options',
                ['questionid' => $this->id],
                'single',
                MUST_EXIST)
                ->single;
            $this->answers = [];
            $answersindb = $DB->get_records('question_answers', ['question' => $this->id]);
            foreach ($answersindb as $answerindb) {
                $this->answers[] = new MultichoiceAnswer($answerindb->id);
            }
        } else {
            if (func_num_args() !== 5) {
                throw new InvalidArgumentException('Number of arguments must be 1 for DB reading or 5 for a total construction');
            }
            // Total construction
            parent::__construct($idornull, $name, $question, $categoryid);
            $this->answers = $answers;
        }

        $this->nbCorrectAnswers = 0;
        foreach ($this->answers as $answer) {
            if ($answer->is_correct()) {
                $this->nbCorrectAnswers++;
            }
            $answer->set_contextid($this->get_contextid());
        }
    }

    protected function qtype(): string
    {
        return 'multichoice';
    }

    protected function file_area(): string
    {
        return 'questiontext';
    }

    /**
     * Layout options
     * @param $options
     */
    protected function fill_options_for_saving($options): void
    {
        $options->layout = 0;
        $options->single = $this->singleanswer;
        $options->answernumbering = 'abc';
    }

    /**
     * Save answers
     */
    protected function save_specific_parts(): void
    {
        $this->compute_answer_fraction();
        foreach ($this->answers as $answer) {
            $answer->save($this->id);
        }
    }

    /**
     * Compute fraction so that getting all right answers with no wrong ones will get you the maximal grade.
     */
    public function compute_answer_fraction(): void {
        $fractionRightAnswer = 1.0 / ((float)$this->nbCorrectAnswers);
        $nbWrongAnswer = count($this->answers) - $this->nbCorrectAnswers;
        if ($nbWrongAnswer !== 0) {
            $fractionWrongAnswer = - 1.0 / ((float)($nbWrongAnswer));
        } else {
            // Normally unused in this case, done to avoid compilation warning
            $fractionWrongAnswer = -1;
        }

        foreach ($this->answers as $answer) {
            if ($answer->is_correct()) {
                $answer->fraction = $fractionRightAnswer;
            } else {
                $answer->fraction = $fractionWrongAnswer;
            }
        }
    }

    /**
     * Check if question is complete and valid before saving.
     * @return bool
     */
    public function is_valid(): bool {
        return count($this->answers) > 1 && $this->nbCorrectAnswers > 0;
    }

    /**
     * Retrieving right answers.
     * @return array of MultichoiceAnswer
     */
    public function get_correct_answers(): array {
        $correctanswers = [];
        foreach ($this->answers as $answer) {
            if ($answer->is_correct()) {
                $correctanswers[] = $answer;
            }
        }
        return $correctanswers;
    }

    /**
     * Overriding update_file to update answers too.
     *
     * @param stored_file $storedfile
     * @param file_storage|null $fs
     * @throws \file_exception
     * @throws \stored_file_creation_exception
     */
    public function update_file(stored_file $storedfile, file_storage $fs = null): void
    {
        parent::update_file($storedfile, $fs);
        foreach ($this->answers as $answer) {
            $answer->update_file($storedfile, $fs);
        }
    }

    /**
     * @return array of MultichoiceAnswer objects
     */
    public function get_answers(): array
    {
        return $this->answers;
    }

    /**
     * @param bool $singleanswer
     */
    public function set_singleanswer(bool $singleanswer): void
    {
        $this->singleanswer = $singleanswer;
    }
}