<?php

namespace vikimodule\multichoice;

use vikimodule\Editor;
use vikimodule\ElementWithTextEditor;
use stdClass;

class MultichoiceAnswer extends ElementWithTextEditor
{
    public $fraction;
    private $iscorrect;

    public function __construct($id, Editor $text = null, $iscorrect = false) {
        if (func_num_args() === 1) {
            // Load case
            global $DB;
            $answerdb = $DB->get_record('question_answers', ['id' => $id], '*', MUST_EXIST);

            $text = new Editor($answerdb->answer, $answerdb->answerformat, $id);
            parent::__construct($id, $text);
            $this->iscorrect = $answerdb->fraction > 0;
        } else {
            // Total construct case
            parent::__construct($id, $text);
            $this->iscorrect = $iscorrect;
        }
    }

    protected function file_area(): string
    {
        return 'answer';
    }

    /**
     * Valid if not empty. A single image is allowed.
     * @return bool
     */
    public function is_valid(): bool {
        return !$this->editor->is_empty();
    }

    /**
     * Delete an answer from the DB.
     * @throws \dml_exception
     */
    public function delete() {
        if (!empty($this->id)) {
            global $DB;
            $DB->delete_records('question_answers', ['id' => $this->id]);
        }
    }

    /**
     * Setter of iscorrect field.
     * @param $newiscorrect
     */
    public function set_iscorrect($newiscorrect) {
        $this->iscorrect = $newiscorrect;
    }

    /**
     * Accessor of iscorrect field.
     * @return bool
     */
    public function is_correct()
    {
        return $this->iscorrect;
    }

    /**
     * @param int $id
     */
    public function set_id(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int ID of the answer in DB
     */
    public function get_id(): int {
        return $this->id;
    }


    /**
     * Save an answer setting the question id field on the fly.
     * @param $questionid
     * @throws \dml_exception
     */
    public function save($questionid) {
        global $DB;

        $answerdb = new stdClass();
        $answerdb->id = $this->id;
        $answerdb->question =  $questionid;
        $answerdb->answerformat = $this->editor->format;
        $answerdb->fraction = $this->fraction;
        $answerdb->feedback = '';
        $answerdb->feedbackformat = FORMAT_HTML;

        if (empty($this->id)) {
            $answerdb->answer = $this->editor->text; // Will be updated with files but needed for initializing DB
            $this->id = $DB->insert_record('question_answers', $answerdb);
            $answerdb->id = $this->id;
        }

        // Update with files
        $answerdb->answer = $this->editor->save_file_draft_area($this->id);
        $DB->update_record('question_answers', $answerdb);
    }

    /**
     * Accessor to answer text field.
     * @return Editor
     */
    public function get_answer(): Editor {
        return $this->editor;
    }
}