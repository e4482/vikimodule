<?php

namespace vikimodule;

use file_storage;
use stored_file;
use stored_file_creation_exception;

abstract class ElementWithTextEditor {

    /**
     * @var int in DB
     */
    protected $id;

    /**
     * @var Editor editor field
     */
    protected $editor;

    /**
     * @var int contextid for file reading
     */
    private $contextid;

    /**
     * @return string file area for file saving
     */
    protected abstract function file_area(): string;

    public function __construct($id, Editor $editor)
    {
        $this->id = $id;
        $this->editor = $editor;
        $this->editor->set_filearea($this->file_area());
    }

    /**
     *
     * Create a file if it is actually present in the question.
     *
     * @param stored_file $storedfile file to be uppdated if present in this element
     * @param file_storage|null $fs file storage object, may be automatically retrieved
     * @throws \file_exception
     * @throws \stored_file_creation_exception
     */
    public function update_file(stored_file $storedfile, file_storage $fs = null): void {

        if (empty($fs)) {
            $fs = get_file_storage();
        }

        $filename = "@@PLUGINFILE@@" . $storedfile->get_filepath() . $storedfile->get_filename();

        if (strpos(urldecode($this->editor->text), $filename) !== false) {
            // Copy the file reference for question bank
            $newdata = [
                'contextid' => $this->contextid,
                'component' => 'question',
                'filearea'=> $this->file_area(),
                'itemid' => $this->id];
            try {
                $fs->create_file_from_storedfile($newdata, $storedfile);
            } catch (stored_file_creation_exception $e) {
                mtrace("File " . $storedfile->get_filename() . " already created");
            }

        }
    }

    /**
     * @param int $contextid
     */
    public function set_contextid(int $contextid): void
    {
        $this->editor->set_contextid($contextid);
        $this->contextid = $contextid;
    }

    /**
     * @return int
     */
    public function get_contextid(): int
    {
        return $this->contextid;
    }

    public function get_id(): int {
        return $this->id;
    }

    /**
     * Clone also the editor.
     */
    public function __clone()
    {
        $this->editor = clone $this->editor;
    }
}