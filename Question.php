<?php

namespace vikimodule;

global $CFG;
require_once($CFG->libdir.'/questionlib.php');
use context_module;
use moodle_exception;
use question_edit_contexts;
use stdClass;

/**
 * Class Question
 * Represent a quiz question seen with common parameters for simple Elea modules.
 * @package mod_epikmatching\local\models\vikimodule
 */
abstract class Question extends ElementWithTextEditor
{
    public const QUESTION_DELIMITER = ';';

    /**
     * @var string name of the question.
     */
    protected $name;
    /**
     * @var int category id of the question.
     */
    protected $categoryid;

    /**
     * @return string qtype for the actual instance of question.
     */
    protected abstract function qtype(): string;

    /**
     * Add specific fields for options in the option object
     * @param $options
     */
    protected abstract function fill_options_for_saving($options): void;

    /**
     * Save other things like answers that are not common.
     */
    protected abstract function save_specific_parts(): void;

    public function __construct($idornull, string $name = null, Editor $question = null, int $categoryid = 0)
    {
        global $DB;

        if (func_num_args() === 1) {
            $questionindb = $DB->get_record('question', ['id' => $idornull], '*', MUST_EXIST);
            parent::__construct(
                $idornull,
                new Editor($questionindb->questiontext, $questionindb->questiontextformat, $idornull));
            $this->categoryid = $questionindb->category;
            $this->name = $questionindb->name;
        } else {
            parent::__construct($idornull, $question);
            $this->categoryid = $categoryid;
            $this->name = $name;
        }
        $this->name = mb_substr($name, 255);
        $this->set_contextid(
            $DB->get_record('question_categories', ['id' => $this->categoryid], 'contextid', MUST_EXIST)->contextid);
    }

    /**
     * Save the question as a question in a question bank.
     * @return bool|int id of the saved question
     * @throws \dml_exception
     */
    public function save(int $coursemoduleid, string $stringcomponent): int {
        global $DB, $USER;

        $this->check_context($coursemoduleid);

        if (empty($this->id)) {
            // Unchanged values
            $questiondb = new stdClass();
            $questiondb->qtype = $this->qtype();
            $questiondb->category = $this->categoryid;
            $questiondb->parent = 0;
            $questiondb->generalfeedback = '';
            $questiondb->generalfeedbackformat = FORMAT_HTML;
            $questiondb->defaultmark = 1;
            $questiondb->penalty = 0;
            $questiondb->hidden = 0;
            $questiondb->createdby = $USER->id;
            $questiondb->timecreated = time();

            // Set the unique code.
            $questiondb->stamp = make_unique_id_code();
            $questiondb->version = make_unique_id_code();
        } else {
            $questiondb = $DB->get_record('question', ['id' => $this->id], '*', MUST_EXIST);
        }

        // Updated values
        $questiondb->timemodified = time();
        $questiondb->modifiedBy = $USER->id;
        $questiondb->name = $this->name;
        $questiondb->questiontext = $this->editor->text;
        $questiondb->questiontextformat = $this->editor->format;

        // get ID in case this is a new question
        if (empty($this->id)) {
            $questiondb->id = $DB->insert_record('question', $questiondb);
        }
        $this->id = $questiondb->id;

        // Update text with files
        $questiondb->questiontext = $this->editor->save_file_draft_area($this->id);

        // specific parts like answers for multichoice
        $this->save_specific_parts();

        // Save changes in DB
        $DB->update_record('question', $questiondb);

        // options
        $this->save_options($stringcomponent);

        return $this->id;
    }

    private function save_options(string $stringcomponent): int
    {
        global $DB;

        // Common parts
        $qtypeoptionbd = 'qtype_' . $this->qtype() . '_options';
        $options = $DB->get_record($qtypeoptionbd, ['questionid' => $this->id], '*', IGNORE_MISSING);
        if (empty($options)) {
            $options = new stdClass();
            $options->questionid = $this->id;
        }
        $options->shuffleanswers = true;
        $options->correctfeedback = get_string('correctfeedback', $stringcomponent);
        $options->correctfeedbackformat = FORMAT_HTML;
        $options->partiallycorrectfeedback = get_string('partiallycorrectfeedback', $stringcomponent);
        $options->partiallycorrectfeedbackformat = FORMAT_HTML;
        $options->incorrectfeedback = get_string('incorrectfeedback', $stringcomponent);
        $options->incorrectfeedbackformat = FORMAT_HTML;
        $options->shownumcorrect = false;

        $this->fill_options_for_saving($options);

        if (!empty($options->id)) {
            $options->id = $DB->update_record('qtype_multichoice_options', $options);
        } else {
            $options->id = $DB->insert_record('qtype_multichoice_options', $options);
        }

        return $options->id;
    }

    /**
     * Check if a question is in the right context: the one of the module
     * @param $coursemoduleid
     */
    private function check_context($coursemoduleid): void {
        global $DB;

        // Only check loaded questions
        if (empty($this->id)) return;

        // Get context of category
        $categorycontext = $DB->get_record('question_categories', ['id' => $this->categoryid], 'contextid', MUST_EXIST)->contextid;
        // Compare to course module context
        $coursemodulecontext = $DB->get_record('context', ['contextlevel' => CONTEXT_MODULE, 'instanceid' => $coursemoduleid], 'id', MUST_EXIST)->id;
        if ($coursemodulecontext != $categorycontext) {
            throw new moodle_exception("Question $this->id cannot be accessed in this context ($coursemodulecontext)");
        }
    }

    /**
     * Question text accessor.
     * @return Editor
     */
    public function get_question(): Editor {
        return $this->editor;
    }

    /**
     * Delete question and all relating objects
     */
    public function delete(): void {
        if (!empty($this->id)) {
            question_delete_question($this->id);
        }
    }

    /**
     * @return string
     */
    public function get_name(): string
    {
        return $this->name;
    }
}