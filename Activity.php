<?php


namespace vikimodule;


use context_module;
use dml_exception;
use stdClass;

abstract class Activity
{
    /**
     * @var int ID of the activity, not the course module
     */
    public $id;
    /**
     * @var int course ID
     */
    public $course;
    /**
     * @var string intro field
     */
    public $intro;
    /**
     * @var int intro format
     */
    public $introformat;
    /**
     * @var string
     */
    public $name;
    /**
     * @var float max grade
     */
    public $grade;
    /**
     * @var long
     */
    public $timecreated;
    /**
     * @var long
     */
    public $timemodified;
    /**
     * @var string question IDs alltogether
     */
    public $questionids;

    /**
     * @var Editor intro in Editor format
     */
    private $introeditor;

    /**
     * @var array all questions
     */
    protected $questions;
    /**
     * @var int course module ID
     */
    protected $coursemodule;

    protected abstract function moduleid(): int;
    protected abstract function table(): string;
    protected abstract function component(): string;
    protected abstract function old_file_areas_to_delete(): array;
    protected abstract function save_questions();
    protected abstract function create_from_form_specific(stdClass $form): void;
    protected abstract function load_from_db_specific(stdClass $dbobject): void;
    /** To be deleted after fields are removed */
    public static function fill_deprecated_fields($tobefilled): void {
        // nothing to do if fields are already deleted
    }


    public function __construct($formorid) {
        if (is_int($formorid)) {
            $this->load_from_db($formorid);
        } else {
            $this->create_from_form($formorid);
        }
    }

    private function create_from_form(stdClass $form): void {
        // ID for update
        if (!empty($form->instance)) {
            global $DB;
            $this->id = $form->instance;
            if (empty($form->timecreated)) {
                $this->timecreated = $DB->get_record($this->table(), ['id' => $form->instance], 'timecreated', MUST_EXIST)->timecreated;
            }
        } else {
            $this->id = 0;
            $this->timecreated = time();
        }
        $this->timemodified = time();
        $this->course = $form->course;

        // Global fields
        $this->intro = $form->intro;
        $this->introformat = $form->introformat;
        $this->name = $form->name;
        $this->grade = $form->grade;

        $this->create_from_form_specific($form);
    }

    private function load_from_db(int $instanceid): void {
        global $DB;
        $indb = $DB->get_record($this->table(), ['id' => $instanceid], '*', MUST_EXIST);

        // Copy fields
        $this->id = $indb->id;
        $this->course = $indb->course;
        $this->intro = $indb->intro;
        $this->introformat = $indb->introformat;
        $this->timecreated = $indb->timecreated;
        $this->timemodified = $indb->timecreated;
        $this->name = $indb->name;
        $this->grade = $indb->grade;
        $this->questionids = $indb->questionids;
        $this->update_course_module();

        $this->load_from_db_specific($indb);
    }

    public function save() {
        self::fill_deprecated_fields($this);
        $this->save_questions();

        global $DB;
        // Create record if not already done
        $tablename = $this->table();
        if ($this->id == 0) {
            $this->id = $DB->insert_record($tablename, $this, true);
        } else {
            $DB->update_record($tablename, $this);
        }

        // useful for grade
        $gradefunctionname = "${tablename}_grade_item_update";
        if (function_exists($gradefunctionname)) {
            $gradefunctionname($this);
        } else {
            throw new \coding_exception("Missing function for updating grades $gradefunctionname");
        }
    }

    public function intro_editor(int $contextid): Editor
    {
        if (empty($this->introeditor)) {
            $this->introeditor = new Editor($this->intro, $this->introformat);
            $this->introeditor->set_filearea('intro');
            $this->introeditor->set_contextid($contextid);
        }
        return $this->introeditor;
    }

    public function get_questionids() {
        return explode(Question::QUESTION_DELIMITER, $this->questionids);
    }

    /**
     * @return array of Question objects
     */
    public function get_questions(): array
    {
        return $this->questions;
    }

    protected function update_course_module(): void {
        global $DB;
        if (empty($this->coursemodule)) {
            $this->coursemodule = $DB->get_record(
                'course_modules', ['module' => $this->moduleid(), 'instance' => $this->id],
                'id', MUST_EXIST)->id;
        }
    }

    /**
     * Save questions.
     * If asked, save questionids field in DB.
     *
     * @param bool $updatequestionfield
     * @return void
     * @throws dml_exception
     */
    public function upgrade(bool $updatequestionfield = false): void {
        $this->save_questions();
        if ($updatequestionfield) {
            global $DB;
            $DB->set_field($this->table(), 'questionids', $this->questionids, ['id' => $this->id]);
        }
    }

    /**
     * Copy files for all questions in the
     * @throws dml_exception
     */
    public function update_files(): void {
        global $DB;

        $contextid = context_module::instance($this->coursemodule, MUST_EXIST)->id;
        $files = $DB->get_records('files', ['component' => $this->component(), 'contextid' => $contextid]);
        $fs = get_file_storage();
        // - copy to new place
        foreach ($files as $filerecord) {
            $storedfile = $fs->get_file_instance($filerecord);
            foreach ($this->questions as $question) {
                $question->update_file($storedfile, $fs);
            }
        }
        // - delete old files
        foreach ($this->old_file_areas_to_delete() as $filearea) {
            $fs->delete_area_files($contextid, $filearea);
        }
    }
}