<?php


namespace vikimodule;

defined('MOODLE_INTERNAL') || die();

use moodle_exception;
use moodle_url;
use stdClass;

class CommonQuiz
{
    /**
     * @var stdClass quiz object in DB for Moodle and SQL manipulation.
     * "id" is the course module id, "instance" the quiz object id (like in form)
     */
    private $quiz;

    /**
     * @var int ID of course module before transformation and insertion
     */
    private $oldcmid;

    /**
     * @var int ID of course module instance before transformation and insertion
     */
    private $oldcminstance;

    /**
     * @var bool
     */
    private $isnewcourse;

    /**
     * @var int
     */
    private $courseid;

    /**
     * @var int
     */
    private $todelete;

    /**
     * CommonQuiz constructor.
     * @param $cmid int old course module id
     */
    public function __construct(int $cmid, $params = [])
    {
        global $DB;

        $quizconfig = get_config('quiz');
        $modules = $DB->get_records('modules');

        // initialize course module data with old ones
        $this->quiz = $DB->get_record('course_modules', ['id' => $cmid], '*', MUST_EXIST);
        $this->oldcmid = $this->quiz->id;
        $this->oldcminstance = $this->quiz->instance;
        $modulename = $DB->get_record('modules', ['id' => $this->quiz->module], 'name', MUST_EXIST)->name;
        $this->quiz->modulename = 'quiz';
        $this->quiz->cmidnumber = ''; // New module
        // Simple copy of activity structure
        $module = $DB->get_record($modulename, ['id' => $this->quiz->instance], '*', MUST_EXIST);
        $this->quiz->course = $module->course;
        $this->quiz->name = $module->name;
        $this->quiz->intro = $module->intro;
        $this->quiz->introformat = $module->introformat;
        // grades conversion
        $this->quiz->sumgrades = $module->grade;
        $this->quiz->grade = $module->grade;
        if (!empty($module->grademin)) {
            $this->quiz->completionpass = $module->grademin;
        } else {
            $this->quiz->completionpass = 0;
        }
        $this->quiz->completionattemptsexhausted = 0;
        $this->quiz->grademethod = QUIZ_ATTEMPTLAST;
        $this->quiz->attempts = $quizconfig->attempts;
        // timing
        $this->quiz->timeopen = 0;
        $this->quiz->timeclose = 0;
        $this->quiz->timelimit = $quizconfig->timelimit;
        $this->quiz->overduehandling = $quizconfig->overduehandling;
        $this->quiz->graceperiod = 0;
        // layout
        $this->quiz->questionsperpage = $quizconfig->questionsperpage;
        $this->quiz->navmethod = QUIZ_NAVMETHOD_SEQ;
        // questions behaviour
        $this->quiz->shuffleanswers = true;
        $this->quiz->preferredbehaviour = 'immediatefeedback';
        $this->quiz->canredoquestions = $quizconfig->canredoquestions;
        $this->quiz->attemptonlast = $quizconfig->attemptonlast;
        // review option
        $this->quiz->attemptduring = 1;
        $this->quiz->specificfeedbackduring = 1;
        $this->quiz->marksimmediately = 1;
        // display
        $this->quiz->showuserpicture = $quizconfig->showuserpicture;
        $this->quiz->decimalpoints = $quizconfig->decimalpoints;
        $this->quiz->questiondecimalpoints = $quizconfig->questiondecimalpoints;
        $this->quiz->showblocks = $quizconfig->showblocks;
        $this->quiz->quizpassword = $quizconfig->password;
        $this->quiz->subnet = $quizconfig->subnet;
        $this->quiz->delay1 = $quizconfig->delay1;
        $this->quiz->delay2 = $quizconfig->delay2;
        $this->quiz->browsersecurity = $quizconfig->browsersecurity;
        // time
        $this->quiz->timecreated = time();
        $this->quiz->timemodified = time();
        // completion
        $this->quiz->completionusegrade = 1;

        // Specific params
        foreach ($params as $field => $value) {
            $this->quiz->$field = $value;
        }

        // Course id by default
        $this->courseid = $this->quiz->course;
    }

    public function add_quiz_to_course($course, $section = null): int {
        global $DB;

        if ($section == null) {
            $section = 0;
        }
        $this->quiz->section = $section;

        [$module, $context, $cw] = can_add_moduleinfo($course, $this->quiz->modulename, $section);
        $this->quiz->module = $module->id;
        $moduleinfo = add_moduleinfo($this->quiz, $course, null);
        // Update with IDs of new generated modules
        $this->quiz->instance = $moduleinfo->id;
        $this->quiz->id = $moduleinfo->coursemodule;

        return $moduleinfo->coursemodule;
    }

    /**
     * Add quiz to course with the section
     * @param stdClass $course in DB
     * @param int $previsouscmid where to put the new quiz activity, by default (if set to 0), after the old one
     * @return int created course module id
     * @throws \dml_exception
     */
    public function switch_to_quiz_in_course(stdClass $course, int $previsouscmid = 0): int {
        global $DB;

        if (empty($previsouscmid)) {
            $previsouscmid = $this->oldcmid;
        }

        $section = $DB->get_record('course_sections', ['id' => $this->quiz->section], '*', MUST_EXIST);

        $this->add_quiz_to_course($course, $section->section);

        // Setting this new module at the right place in the section
        $sequence = explode(',', $section->sequence);
        $indexSequence = array_search($previsouscmid, $sequence);
        $sequence[$indexSequence] = $previsouscmid . ',' . $this->quiz->id;
        $section->sequence = implode(',', $sequence);
        $DB->update_record('course_sections', $section);
        rebuild_course_cache($course->id);

        // To remember for cleaning
        $this->isnewcourse = false;
        $this->courseid = $course->id;
        $this->todelete = $this->oldcmid;

        return $this->quiz->id;
    }

    /**
     * Create a single activity course from another one
     * @param stdClass $oldcourse
     * @throws \coding_exception
     * @throws \dml_exception
     * @throws moodle_exception
     */
    public function create_single_activity_course(stdClass $oldcourse): void {
        global $DB;

        $newcourse = $this->create_course($oldcourse, 'singleactivity');

        // Add quiz module
        $this->add_quiz_to_course($newcourse);
    }



    /**
     * @param stdClass $oldcourse
     * @param $format
     * @param string $activitytype
     * @throws \coding_exception
     * @throws moodle_exception
     */
    public function create_course(stdClass $oldcourse, $format) {
        global $DB, $USER;

        // if to be deleted (for now, deactivated, may be an option later)
        $this->todelete = $oldcourse->id;

        // Create course from old one
        unset($oldcourse->id);
        unset($oldcourse->shortname);
        unset($oldcourse->idnumber);
        unset($oldcourse->timecreated);
        unset($oldcourse->timemodified);
        $oldcourse->format = $format;
        switch ($format) {
            case 'singleactivity':
                $oldcourse->activitytype = 'quiz';
                break;
            case 'topics':
                break;
            default:
                throw new moodle_exception("Unexpected format course : $format");
        }

        $newcourse = create_course($oldcourse);

        // enrol user with editingteacher role
        $enrolData = $DB->get_record('enrol', array('enrol'=>'manual', 'courseid'=>$newcourse->id));
        $enrol_manual = enrol_get_plugin('manual');
        $editingteacherid = $DB->get_record('role', ['shortname' => 'editingteacher'], 'id', MUST_EXIST)->id;
        $coursecreatorid = $DB->get_record('role', ['shortname' => 'coursecreator'], 'id', MUST_EXIST)->id;
        $enrol_manual->enrol_user($enrolData, $USER->id, $editingteacherid);
        $enrol_manual->enrol_user($enrolData, $USER->id, $coursecreatorid);

        $this->courseid = $newcourse->id;

        // To remember for cleaning
        $this->isnewcourse = true;
        return $newcourse;
    }

    /**
     * @param $questionids array of question IDs added to the quiz
     * @param int $slotsperpage
     * @throws \dml_exception
     */
    public function add_questions(array $questionids, int $slotsperpage = 0): void {
        global $DB;

        foreach ($questionids as $indexSlot => $questionid) {
            $slot = new stdClass();
            $slot->slot = $indexSlot + 1;
            $slot->quizid = $this->quiz->instance;
            $slot->page = $slot->slot;
            $slot->requireprevious = 0;
            $slot->questionid = $questionid;
            $slot->maxmark = $this->quiz->sumgrades / count($questionids);

            $DB->insert_record('quiz_slots', $slot);
        }

        quiz_repaginate_questions($this->quiz->instance, $slotsperpage);
    }

    public function get_course_id(): int {
        return $this->courseid;
    }

    /**
     * @return int
     */
    public function get_old_cm_instance(): int
    {
        return $this->oldcminstance;
    }
}