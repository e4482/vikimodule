<?php


namespace vikimodule;


use context;

class Editor
{
    /**
     * @var string Raw text
     */
    public $text;
    /**
     * @var int text format (Moodle constant)
     */
    public $format;
    /**
     * @var int item id which is draft one or stored file one depending if saving or loading
     */
    public $itemid;

    /**
     * @var string file area
     */
    private $filearea;
    /**
     * @var int contextid linked to question category
     */
    private $contextid;

    public function __construct($text, $format = FORMAT_HTML, $itemid = null)
    {
        $this->text = $text;
        $this->format = $format;
        $this->itemid = $itemid;
    }

    /**
     * @return bool true if the text is not valid (empty, only white spaces, ...)
     */
    public function is_empty(): bool {
        return empty(strip_tags($this->text, '<img><iframe><audio><video>'));
    }

    /**
     * Prepare file area with common arguments using classical file function
     *
     * @param int $draftitemid
     * @param string $editorname
     * @return string text formatted with files put in draft area
     */
    public function prepare_file_draft_area(&$draftitemid): string {
        return file_prepare_draft_area(
            $draftitemid,
            $this->contextid,
            'question',
            $this->filearea,
            $this->itemid,
            ['maxfiles' => EDITOR_UNLIMITED_FILES],
            $this->text);
    }

    /**
     * Save file using classical file function and returning text to be saved.
     *
     * @param $itemid int used to store info as itemid of this object is draft one
     * @return string formatted text with stored files
     */
    public function save_file_draft_area($itemid): string {
        if ($this->itemid) {
            return file_save_draft_area_files($this->itemid, $this->contextid, 'question',
                $this->filearea, $itemid, null, $this->text);
        }
        return $this->text;
    }

    /**
     * @param mixed $contextid
     */
    public function set_contextid($contextid): void
    {
        $this->contextid = $contextid;
    }

    /**
     * @param mixed $filearea
     */
    public function set_filearea($filearea): void
    {
        $this->filearea = $filearea;
    }

    /**
     * @return int context id, generally of a question category so a course category
     */
    public function get_context_id(): int
    {
        return $this->contextid;
    }

    /**
     * @return string file area
     */
    public function get_file_area(): string
    {
        return $this->filearea;
    }

    public function reduce_size(): void {
        $this->text = str_replace("<br />", "", $this->text);
        $this->text = str_replace("<br/>", "", $this->text);
        $this->text = str_replace("<br>", "", $this->text);
        $this->text = str_replace("&nbsp;", "", $this->text);
        $this->text = str_replace("<p>", "", $this->text);
        $this->text = str_replace("</p>", "", $this->text);
    }

    /**
     * @var array of filters
     */
    private static $active_filters;
    private static function get_active_filters(): array {
        if (empty(self::$active_filters)) {
            global $DB;
            self::$active_filters = $DB->get_records('filter_active', ['active' => 1], 'sortorder');
        }
        return self::$active_filters;
    }

    /**
     * Filter text of this editor with
     * - file url pluginfile
     * - active filters
     * @param int $coursemodulecontextid
     * @return string modidied $this->text
     */
    public function get_filtered_content(context $coursemodulecontext, string $component): string {
        global $CFG;

        $content = file_rewrite_pluginfile_urls(
            $this->text, 'pluginfile.php', $this->contextid,
            $component, $this->filearea, $this->itemid);

        foreach (self::get_active_filters() as $currentfilter) {
            if (file_exists($CFG->dirroot . '/filter/' . $currentfilter->filter . '/filter.php')) {
                require_once($CFG->dirroot . '/filter/' . $currentfilter->filter . '/filter.php');
                $class_filter = "filter_" . $currentfilter->filter;
                $filterplugin = new $class_filter($coursemodulecontext, array());
                $original_content = $content;
                $content = $filterplugin->filter($original_content, array('originalformat' => '0'));
            }
        }
        return $content;
    }

}